#!/usr/bin/python
# Filename: Generate-Polycom-Cookie.py
# -*- coding: utf-8 -*-

import base64

def Generate_Polycom_cookie(User='Polycom', Password='456', Debug=True):
    ''' Returns Polycom phone cookie data. User can be Admin or User. '''
    String = User+':'+Password
    Encoded_String = base64.b64encode(String)
    if Debug == True:
        print '\n[+] Creating cookie from:', String
        print '[+] Cookie:', Encoded_String
    return Encoded_String
