#!/usr/bin/python
# Filename: pyautogui_Example.py
# -*- coding: utf-8 -*-

import pyautogui

Images_Directory = r'C:\Images\\'

# Find image on screen
SAP_Window = pyautogui.locateOnScreen(Images_Directory+'SAP_Window.png')

# Move mouse
pyautogui.moveTo(SAP_Window[0]+2,
                 SAP_Window[1]+2,
                 duration=.6)

# Click mouse     
pyautogui.click()

# Drag mouse
pyautogui.dragTo(1005, 835, 2, button='left')





