#!C:\Python27\python.exe
# Filename: Get_MouseXY_RBG.py
# -*- coding: utf-8 -*-

import win32api, time
import pyscreenshot as ImageGrab
from PIL import Image

''' Get RBG color values from XY coordinate on the screen

 Get pixel example
 try:
     print '[+] Set RBG:', im.getpixel((216, 269))
 except:
     print '[!] Set RBG: FAILED (216, 269)\n\n'

'''

def Get_Mouse_Coordinates_and_RBG():
    ''' Get, return cursor X, Y coordinates & pixel color '''
    for i in range(10000):
        time.sleep(1)
        MouseXY = win32api.GetCursorPos()
        print '\n[+] Cursor XY: '+str(MouseXY)
        
Get_Mouse_Coordinates_and_RBG()
