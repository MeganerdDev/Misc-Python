#!C:\Python27\python.exe
# Filename: FTPServer.py
# -*- coding: utf-8 -*-

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

authorizer = DummyAuthorizer()
authorizer.add_anonymous(r'C:\Users\Meganerd\Documents\FTPROOT\\')
handler = FTPHandler
handler.authorizer = authorizer

server = FTPServer(('127.0.0.1', 9001), handler)

server.serve_forever()
