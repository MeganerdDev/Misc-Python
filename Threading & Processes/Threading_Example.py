#!/usr/bin/python
# Filename: Threading_Example.py
# -*- coding: utf-8 -*-

import threading
import time

def Start_Thread(Thread):
    ''' Start worker thread '''
    global t
    t = threading.Thread(target=Thread)
    t.start()


def Example_Thread():
    for i in range(0, 10):
        print i
        time.sleep(1)
    print 'Example_Thread fin'
    return

def Example_Thread_With_Args(Arg_1, Arg_2):
    for i in range(20, 30):
        print i
        print Arg_1
        print Arg_2
        time.sleep(1)
    print 'Example_Thread_With_Args fin'
    return

if __name__ == "__main__":
    Start_Thread(Example_Thread)
    Start_Thread(lambda: Example_Thread_With_Args(Arg_1='a', Arg_2='b'))
    
    '''
    If youre running a thread from TKinter button
    
    lambda: Start_Thread(Example_Thread)
    lambda: Start_Thread(lambda: Example_Thread_With_Args(Arg_1='a', Arg_2='b'))
    '''