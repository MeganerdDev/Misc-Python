#!/usr/bin/env python
# Filename: Python_Send_File.py
# -*- coding: utf-8 -*-
import os
import socket
import sys

''' Client '''

# Create a TCP/IP socket
global sock; sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
global server_address; server_address = ('192.168.1.1', 10000)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)

try:
    #message = 'SNSNSNSNSNSN,MACMACMACMACMA,MODELMODEL,192.168.10.20' + '#' * 150
	file = open('/home/megaman/Documents/VM SHARED/GIT/Atsu/mykey.pem', 'r')
	for line in file:
		message = line
		sock.sendall(str(line))
		print line

    # Look for the response
	amount_received = 0
	amount_expected = len(message)

	while amount_received < amount_expected:
		data = sock.recv(256)
		amount_received += len(data)
		print >>sys.stderr, 'received "%s"' % data

finally:
	print >>sys.stderr, 'closing socket'
	sock.close()
