#!/usr/bin/env python
# Filename: Plotly_Bar_Graph_Example.py
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

# data to plot
Number_of_groups = 13
Data = [ # [Method, File, (Ratios)]
        ['A', 'LI.class', (90, 55, 40, 65, 1, 2, 3, 4, 5, 6, 0, 1, 2)],
        ['B', 'LI.class', (85, 62, 54, 20, 1, 2, 3, 4, 5, 6, 0, 1, 2)],
        ['C', 'LI.class', (85, 62, 54, 20, 1, 2, 3, 4, 5, 6, 0, 1, 2)],
        ]

# create plot
fig, ax = plt.subplots()
index = np.arange(Number_of_groups)
bar_width = 0.1
opacity = 0.8

rects1 = plt.bar(index, Data[0][2], bar_width,
                 alpha=opacity,
                 color='b',
                 label='A, LI.class')

rects2 = plt.bar(index + bar_width, Data[1][2], bar_width,
                 alpha=opacity,
                 color='g',
                 label='B, LI.class')

rects3 = plt.bar(index + bar_width*2, Data[2][2], bar_width,
                 alpha=opacity,
                 color='y',
                 label='C, LI.class')

plt.xlabel('Bytecode Method')
plt.ylabel('Similarity Ratio')
plt.title('Bytecode Analysis')
plt.xticks(index + bar_width, ('createFrame', '\nwriteDWord', 'writeWordBigEndian',
                               '\nwriteWord', 'writeDWordBigEndian', '\nmethod403',
                               'writeQWord', '\nwriteString', 'method424',
                               '\nmethod425', 'method431', '\nmethod432', 'method433'))
plt.legend()

plt.tight_layout()
plt.show()
