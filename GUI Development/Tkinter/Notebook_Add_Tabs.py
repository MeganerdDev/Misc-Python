#!/usr/bin/env python
# Filename: Notebook_Add_Tabs.py
# -*- coding: utf-8 -*-

import Tkinter

from ttk import Notebook
from ttk import Treeview
import ttk

class Notebook_Class:

    def __init__(self, x, y, title):
        self.Frames_ = []
        self.notebook = Notebook(GUI)
        #self.Main_Tab = Frame(self.notebook, width=1600, height=875)
        #self.notebook.add(self.Main_Tab, text=title, compound=TOP)
        self.notebook.place(x=x, y=y)

    def Add_Tab(self, Text, i):
        self.Frames_[i] = Frame(self.notebook, width=1600, height=875)
        self.notebook.add(self.Frames_[i], text=Text)

GUI_Notebook = Notebook_Class(x=0, y=0, title='Main') # Create Notebook (TABS) for GUI

GUI_Notebook.Frames_ = ['Main_Tab', 'Tab1', 'Tab2', 'Tab3']

for i in range(len(GUI_Notebook.Frames_)):
    GUI_Notebook.Frames_[i] = GUI_Notebook.Add_Tab(Text=GUI_Notebook.Frames_[i], i=i)

