#!/usr/bin/python
# Filename: Threaded_Tk_GUI_Example.py
# -*- coding: utf-8 -*-

import time
import threading

try:
    from mtTkinter import *
except:
    print '\n[!] mtTkinter import failed - threads are not stable!'
    from Tkinter import *

#import Tkinter as tk
#import tkMessageBox
import Tkinter
#import tkFileDialog
#from Tkinter import Canvas

# ttk, Notebook
#from ttk import Notebook
#from ttk import Treeview
#import ttk

# Pillow
#from PIL import Image
#from PIL import ImageTk

def Start_Thread(Thread):
    ''' Start worker thread '''
    global t
    t = threading.Thread(target=Thread)
    t.start()

def Hello_Universe():
    print 'Hello Universe'
    return

# TK GUI
GUI = Tk() # CREATE GUI (TKINTER)
GUI.title('Threaded Tk GUI Example') # Set window name
GUI.geometry('1140x720') # Set window size
GUI.resizable(width = FALSE, height = FALSE) # Make window not resizable

# TK Style
'''
Style = ttk.Style()
#if OS.upper() == 'WINDOWS':
#    Style.theme_use('winnative') # Set GUI theme for Windows machines
try:
    Not_Selected = '#A4CBEE'
    Selected = '#FFFFFF'

    Style.theme_create( 'ExampleStyle', parent='alt', settings={
            'TNotebook': {'configure': {'tabmargins': [2, 5, 2, 0] } },
            'TNotebook.Tab': {
                'configure': {'padding': [5, 1], 'background': Not_Selected },
                'map':       {'background': [('selected', Selected)],
                              'expand': [('selected', [1, 1, 1, 0])] } } } )

    Style.theme_use('ExampleStyle')
except:
    print '\n[!] Unable to apply custom GUI theme (ttk)'
'''

# Menu bar
Menu_Bar = Menu(GUI)

File_Menu = Menu(Menu_Bar, tearoff=0)
File_Menu.add_command(label='Hello Universe', command=Hello_Universe)
File_Menu.add_separator()
File_Menu.add_command(label='Exit', command=GUI.quit)
Menu_Bar.add_cascade(label='File', menu=File_Menu)



# Notebook
'''
Services_Notebook = Notebook(Services_Tab) # Create Notebook (TABS) for Service Manager tab

Overview_Tab = Frame(Services_Notebook, width = 1115, height = 634)
DHCP_Server_Tab_ = Frame(Services_Notebook, width = 1115, height = 634)
FTP_Server_Tab_ = Frame(Services_Notebook, width = 1115, height = 634)
TFTP_Server_Tab_ = Frame(Services_Notebook, width = 1115, height = 634)
HTTP_Server_Tab_ = Frame(Services_Notebook, width = 1115, height = 634)
SSH_Server_Tab_ = Frame(Services_Notebook, width = 1115, height = 634)

Services_Notebook.add(Overview_Tab, text = '                     Overview                     ', compound=TOP)
Services_Notebook.add(DHCP_Server_Tab_, text = 'DHCP   ')
Services_Notebook.add(FTP_Server_Tab_, text = 'FTP   ')
Services_Notebook.add(TFTP_Server_Tab_, text = 'TFTP   ')
Services_Notebook.add(HTTP_Server_Tab_, text = 'HTTP   ')
Services_Notebook.add(SSH_Server_Tab_, text = 'SSH   ')

Services_Notebook.place(x = 0, y = 0) # Place Notebook
'''


# Background
'''
try:
    BG_Image = 'BG.png'
    Background_Image = ImageTk.PhotoImage(Image.open(BG_Image))
    Background_label = Label(GUI, image=Background_Image)
    Background_label.place(x=-1, y=-1, relwidth=1, relheight=1)
    Background_label.image = Background_Image
    Background_label.lower()
except:
    print '\n[!] Unable to find background: %s' % BG_Image
'''

Button_Background_Color = '#A4CBEE' # BG fill color
Button_Foreground_Color = 'black' # Text color

Example_Button = Button(Polycom_Provisioning_Server_Configuration, text='Hello Universe',
                                width = 12, height = 2,
                                fg=Button_Foreground_Color, bg=Button_Background_Color,
                                font = 'arial 12', command = lambda: Start_Thread(Hello_Universe))
Example_Button.place(x = 200, y = 200)


if __name__ == '__main__':
    GUI.mainloop()