#!/usr/bin/python
# Filename: Remove_Duplicate_Files.py
# -*- coding: utf-8 -*-

import sys
import os
import hashlib
import md5

Path = r'C:\Files\\'

def remove_duplicates(Directory):
    Unique_File_List = []
    for Filename in os.listdir(Directory):
        if os.path.isfile(Directory+str(Filename)):
            #print str(Filename)
            File_Hash = md5.md5(file(Directory+str(Filename)).read()).hexdigest()
            if File_Hash not in Unique_File_List: 
                Unique_File_List.append(File_Hash)
            else:
                print '[+] Removing duplicate file %s' % str(Filename)
                os.remove(Directory+str(Filename))

if __name__ == "__main__":
    remove_duplicates(Directory=Path)
