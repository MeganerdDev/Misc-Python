#!/usr/bin/python
# Filename: HTTPServer.py
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import SimpleHTTPServer
    import SocketServer

    PORT = 80
    Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
    httpd = SocketServer.TCPServer(('', PORT), Handler)

    print '[+] HTTP running on port', PORT
    httpd.serve_forever()