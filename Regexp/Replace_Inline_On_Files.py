#!/usr/bin/python
# Filename: Replace_Inline_On_Files.py
# -*- coding: utf-8 -*-

import os
import re
import time

Path = r'C:\Files\\'
Filenames = []

for file in os.listdir(Path):
    #print str(file)
    Filenames.append(str(file))

for filename in Filenames:
    if 'FES' in str(filename) and not 'Fixed_' in str(filename):
        print '\n[+] Opening file %s' % str(filename)
        F = open(Path+filename, 'r')
        Data = F.read()
        F.close()

        #print Data
        #a = raw_input('\n')
        
        Data = re.sub('priviledge', 'privilege', Data)
        Data = re.sub('configure ten-gig port', '!', Data)
        Data = re.sub('te1\/0\/1', 'te1/1/1', Data)
        Data = re.sub('=', '', Data)

        Data = re.sub('line con 0\n login local', '!', Data)
        Data = re.sub('login local', 'login authentication admin', Data)
        Data = re.sub('system mtu routing 1500', 'system mtu 1500', Data)

        #print Data
        #a = raw_input('\n')

        F = open(Path+'Fixed_'+filename, 'w')
        F.write(str(Data))
        F.close()

        print '[+] Saved file: \n%s' % str(Path+'Fixed_'+filename)
    
