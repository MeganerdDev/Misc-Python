#!/usr/bin/python
# Filename: POC-Read-DHCP-Export
# -*- coding: utf-8 -*-

import re

Regex_IP = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

print '\n[+] Drop DHCP export file in this window'
Export_File = raw_input('\n[+] ')
if '\\' in str(Export_File):
    Export_File = Export_File.replace('\\', '\\\\')
File = open(Export_File, 'r')
Data = File.readlines()
IPS = re.findall(Regex_IP, str(Data))

print '\n[+] Got %d IP addresses from file' % len(IPS)
