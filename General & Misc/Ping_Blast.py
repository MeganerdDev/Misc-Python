#!C:\Python27\python.exe
# Filename: Ping_Blast.py
# -*- coding: utf-8 -*-
import re
import subprocess
import datetime as Date
import os.path

Enable_Logging = True
Regex_ICMP_Reply = 'Reply from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'

def Create_Arg_Dir(Directory):
    try:
        if len(str(Directory)) < 250:
            if not os.path.exists(Directory):
                os.makedirs(Directory)
                print '[+] Directory ', str(Directory), 'created.'
        else:
            print '[!] Directory name too long for Windows file-system'
    except:
        print '[!] Got exception: While trying to create', str(Directory)

def Log(Data):
    FILE = 'PingBlast_'+str(Date.date.today())+'.txt'
    try:
        Log_ = open(FILE, 'a+')
    except:
        try:
            for i in range(1,10):
                Log_ = open(str(i)+'_'+FILE+str(Date.date.today())+'.txt', 'a+')
                break
        except:
            pass

    try:
        Log_.write(str(Data)+'\n')
    except:
        print '\n[!] Got exception while writing to Error Log'
        try:
            Log_.write('\n[!] Got exception while writing to Error Log\n'+str(Date.date.today())+'\n')
        except:
            pass

    try:
        Log_.close()
    except:
        pass

def Ping_Subnet(Subnet):
    ''' Send ICMP to subnet range, return results '''
    Live_Hosts = []
    for i in range(1,254):
        IP = str(Subnet)+'.'+str(i)
        print '\n[*] Pinging:', IP
        Ping_IP = subprocess.Popen(['ping', IP, '-n', '1', '-w', '100'],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        Ping_Out_IP, Ping_Err_IP = Ping_IP.communicate()
        try:
            Ping_Out_IP = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
        except:
            Ping_Out_IP = ''
            print '[-] Host down:', str(IP) # Debugging

        if len(str(Ping_Out_IP)) > 1:
            print '[+] Host up:', str(IP), '***'
            Live_Hosts.append(Ping_Out_IP)

    print '\n', '-' * 45, '\n\n[+] ICMP sweep completed -', len(Live_Hosts), 'hosts up.\n'
    print Live_Hosts, '\n', '-' * 45

    if Enable_Logging == True:
        Log(Live_Hosts)

    return Live_Hosts

def Ping_Subnet_Range(Subnet, Min, Max):
    ''' Send ICMP to subnet range, return results '''
    Live_Hosts = []
    for i in range(Min,Max):
        IP = str(Subnet)+'.'+str(i)
        print '\n[*] Pinging:', IP
        Ping_IP = subprocess.Popen(['ping', IP, '-n', '1', '-w', '100'],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        Ping_Out_IP, Ping_Err_IP = Ping_IP.communicate()
        try:
            Ping_Out_IP = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
        except:
            Ping_Out_IP = ''
            print '[-] Host down:', str(IP) # Debugging

        if len(str(Ping_Out_IP)) > 1:
            print '[+] Host up:', str(IP), '***'
            Live_Hosts.append(Ping_Out_IP)

    print '\n', '-' * 45, '\n\n[+] ICMP sweep completed -', len(Live_Hosts), 'hosts up.\n'
    print Live_Hosts, '\n', '-' * 45

    if Enable_Logging == True:
        Log(Live_Hosts)

    return Live_Hosts

def Ping(IP):
    ''' Send ICMP using to specified IP address, return results '''
    print '\n[*] Pinging:', IP

    # Send ICMP
    Ping_IP = subprocess.Popen(['ping', str(IP), '-n', '1', '-w', '100'],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # Stdout results to variable
    Ping_Out_IP, Ping_Err_IP = Ping_IP.communicate()

    try:
        Ping_Out_IP = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
    except:
        Ping_Out_IP = ''
        print '[-] Host down:', str(IP) # Debugging

    if len(str(Ping_Out_IP)) > 1:
        print '\n', '-' * 45, '[+] Host up:', str(IP), '\n', '-' * 45
        if Enable_Logging == True:
            Log(IP)

    return Ping_Out_IP

def Main_Menu():
    ''' Main option menu '''

    print '''
[1] Ping Subnet (/24)
[2] Ping IP (/24)
[3] Ping Subnet w/ custom range (/24)
'''
    try:
        Choice = int(str(raw_input('[#] '))[0])
    except:
        print '\n[!] Bad input - Retry'
        Main_Menu()

    try:
        if Choice == 1:
            Choice = raw_input('\n[+] Enter subnet (Example: 192.168.1.0)\n[+] ')
            IP = '.'.join(Choice.split('.')[0:3])
            print '\n[+] Results: (live hosts)\n[+]', Ping_Subnet(IP)

        elif Choice == 2:
            IP = raw_input('\n[+] Enter IP (Example: 192.168.1.1)\n[+] ')
            Ping(IP)

        elif Choice == 3:
            Choice = raw_input('\n[+] Enter subnet (Example: 192.168.1.0)\n[+] ')
            IP = '.'.join(Choice.split('.')[0:3])

            Min = int(str(raw_input('\n[+] Enter minimum IP (1 - 253) \n[#] ')))
            if Min > 253:
                print '\n[!] Invalid integer'
                Main_Menu()
            elif Min == 0:
                Min = 1

            Max = int(str(raw_input('\n[+] Enter maximum IP (2 - 254)\n[#] ')))
            if Max > 254:
                print '\n[!] Invalid integer'
                Main_Menu()

            if Min > Max:
                print '\n[!] Error: Min > Max'
                Main_Menu()

            print Ping_Subnet_Range(IP, Min, Max)

    except:
        print '\n[!] Got exception'
        Main_Menu()


Main_Menu()

raw_input('\n\n[+] Press any key to exit\n')







