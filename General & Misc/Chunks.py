#!/usr/bin/python
# Filename: Chunks.py
# -*- coding: utf-8 -*-

def Chunks(List, n):
    ''' Split lists '''
    n = max(1, n)
    return [List[i:i + n] for i in range(0, len(List), n)]

List = []
for i in range(1, 100):
    List.append(i)