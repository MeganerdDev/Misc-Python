#!C:\Python27\python.exe
# Filename: Common.py
# -*- coding: utf-8 -*-
import ftplib
import os
import platform
import re
import subprocess

import time

import GlobalVariables
from ftplib import FTP
#from datetime import datetime
#import datetime as Date

try:
    import paramiko
except:
    print '\n[!] Unable to import paramiko, we will be unable to use SHH/SFTP'


OS = platform.system()

# Regexp
Regex_IP = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
Regex_ICMP_Reply = 'Reply from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'

# [ (IP, MAC), .. ]
Regex_DHCP_Export = '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[  ]+([0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2})'

# Regexp NetSH DHCP Export
# return: (IPv4, Mask, MAC, Lease Expire Time)
Regex_NetSH_DHCP_Export = '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ ]{1,5}-[ ]{0,5}(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[ ]{1,5})-[ ]{0,5}([0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2})[ ]{0,5}-[ ]{0,5}(\d{1,2}/\d{1,2}/\d{2,4}[ ]{0,5}\d{1,2}:\d{1,2}:\d{1,2} [AM|PM]{2})'
Alt_Regex_NetSH_DHCP_Export = '(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ ]{1,5}-[ ]{0,5}(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[ ]{1,5})-[ ]{0,5}([0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2}-[0-9a-fA-F]{2})'


def Ping(IP, Debug=True):
    ''' Send ICMP to IPv4 address, return [Boolean, IP, 'stdout from shell'] '''
    Successful = False
    
    # Check if IP is banned
    if IP in GlobalVariables.Banned_IPs:
        if Debug == True:
            print '\n[*] %s is in banned IP list; We will not ping device', IP
        return [Successful, IP, '']
    
    if Debug == True:
        print '\n[*] Pinging:', IP
    Ping_IP = subprocess.Popen(['ping', str(IP), '-n', '1', '-w', '100'],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    Ping_Out_IP, Ping_Err_IP = Ping_IP.communicate() # Stdout results to variable
    try:
        Ping_Out_IP_Regex = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
    except:
        Ping_Out_IP_Regex = ''
        if Debug == True:
            print '[!] Host down:', str(IP)
            
    if len(str(Ping_Out_IP)) > 1:
        # Check for any errors
        if 'TTL EXPIRED' in str(Ping_Out_IP).upper():
            # Fail; host is down
            if Debug == True:
                print '[!] Host down:', str(IP)
        
        elif 'ERROR' in str(Ping_Out_IP).upper():
            # Fail; host is down
            if Debug == True:
                print '[!] Host down:', str(IP)
        
        elif 'could not find host'.upper() in str(Ping_Out_IP).upper():
            # Fail; host is down
            if Debug == True:
                print '[!] Host down:', str(IP)

        elif 'Request timed out'.upper() in str(Ping_Out_IP).upper():
            # Fail; request timed out
            if Debug == True:
                print '[!] Request timed out:', str(IP)
                
        elif 'bytes=32'.upper() in str(Ping_Out_IP).upper():
            # Success; host is alive
            if Debug == True:
                print '[+] Host up:', str(IP)
            Successful = True
       
    return [Successful, IP, Ping_Out_IP]

def Sanitize_IP(IP='', Debug=True):
    ''' Sanitize IP address from input, return IP or [IP, IP,..] if multiple '''
    Matches = re.findall(IP, Regex_IP)
    if type(Matches) == str and Matches == '':
        if Debug == True:
            print '\n[!] No matching IP addresses found'
        return None
    elif type(Matches) == list and len(Matches) > 0:
        if len(Matches) == 1:
            if Debug == True:
                print '\n[+] %s IP addresses found' % Matches[0]
            return Matches[0]
        elif len(Matches) > 1:
            if Debug == True:
                print '\n[+] %d IP addresses found' % len(Matches)
            return Matches

def Check_File_Exists(filename, Debug=False):
    ''' Input path+filename, returns boolean '''
    if os.path.isfile(filename):
        return True
    else:
        if Debug == True:
            print '[+] File does not exist'
        return False

def Check_Directory_Exists(Directory, Debug=False):
    ''' Input directory path, returns boolean '''
    if os.path.isdir(Directory):
        return True
    else:
        if Debug == True:
            print '[+] Directory does not exist'
        return False

def Create_Directory(Directory, Debug=False):
    ''' Input directory to create, returns boolean '''
    try:
        if OS.upper() == 'WINDOWS':
            if len(str(Directory)) < 250:
                if not os.path.exists(Directory):
                    os.makedirs(Directory)
                    if Debug == True:
                        print '[+] Directory ', str(Directory), 'created.'
                    return True
            else:
                if Debug == True:
                    print '[!] Directory name too long for Windows file-system'
                return False
        elif OS.upper() == 'LINUX':
            if not os.path.exists(Directory):
                os.makedirs(Directory)
                if Debug == True:
                    print '[+] Directory ', str(Directory), 'created.'
                return True
    except:
        if Debug == True:
            print '[!] Got exception: While trying to create', str(Directory)
        return False

def Drop_To_PDB_Shell():
    ''' Drops console window to PDB shell '''
    print '\n\n[*] Dropping to PDB shell - Happy debugging!\n'
    import pdb;pdb.set_trace()

def Str_to_List(String):
    ''' Convert string to a list '''
    List = str(String.replace("'", '').replace('[', '').replace(']', '')).split(', ')
    return List

def Chunks(List, n):
    ''' Split lists to chunks '''
    n = max(1, n)
    return [List[i:i + n] for i in range(0, len(List), n)]

def Print_Global_Variables():
    ''' Print important global variables for debugging '''
    print '\n[GlobalVariables.py]'
    if not GlobalVariables.Site == None:
        print '[+] Current Site: ', GlobalVariables.Site
    print '[+] Database IPv4: ', GlobalVariables.DBIP
    print '[+] Database Server Auth Username: ', GlobalVariables.BDUser
    print '[+] Database Schema: ', GlobalVariables.DBSche
    print '[+] DHCP Server IPv4: ', GlobalVariables.DHCPIP
    print '[+] TFTP Server IPv4: ', GlobalVariables.TFTP_Server_IP
    print '[+] Program Root directory: ', GlobalVariables.Program_Root
    print '[+] Program images directory: ', GlobalVariables.Program_Images
    print '[+] %d Banned IPv4 from scanning' %len(GlobalVariables.Banned_IPs)
    print '[+] Tk commit path: ', GlobalVariables.TK_Commit_Path
    print '\n\n'


def Display_SiteID():
    ''' Display SiteID.txt file contents '''
    if Check_File_Exists('C:\\Python27\\SiteID.txt') == True:
        print '\n[+] SiteID file information'
        print '[+] File path: C:\\Python27\\SiteID.txt'
        File = open('C:\\Python27\\SiteID.txt', 'r')
        File_Data = File.read()
        File.close()
        print '\n', '-' * 3, 'File Contents', '-' * 3
        print '\n', File_Data, '\n'
        print '-' * 21, '\n'
        return
        
    elif Check_File_Exists('C:\\Python27\\SiteID.txt') == False:
        print '[+] No SiteID found in C:\\Python27\\'
        return False

def Create_SiteID():
    ''' Create SiteID.txt file '''
    if Check_File_Exists('C:\\Python27\\SiteID.txt') == True:
        print '[+] SiteID file exists! Full path: C:\\Python27\\SiteID.txt'
        Choice = raw_input('\n[?] Overwrite file?\n[Yy/Nn] ')
        Choice = Choice[0].upper()
        if Choice == 'Y':
            Site_Name = raw_input('\n[+] Please input site name\n[+] ')
        
            Site_File = open('C:\\Python27\\SiteID.txt', 'w+')
            Site_File.write(Site_Name.upper())
            Site_File.close()
            
            print '\n[+] SiteID file overwritten successfully'
            print '\n[+] SiteID file can be found at: C:\\Python27\\SiteID.txt'
            return
        
        else:
            print '[+] You opted to leave the SiteID file\n'
            return
        
    elif Check_File_Exists('C:\\Python27\\SiteID.txt') == False:
        print '[+] No SiteID found in C:\\Python27\\'
        print '\n[+] We will create a SiteID file for this system'
    
        Site_Name = raw_input('\n[+] Please input site name\n[+] ')
    
        Site_File = open('C:\\Python27\\SiteID.txt', 'w+')
        Site_File.write(Site_Name.upper())
        Site_File.close()
        
        print '\n[+] SiteID file created successfully'
        print '\n[+] SiteID file can be found at: C:\\Python27\\SiteID.txt'
        return

def Help():
    ''' Help menu '''
    print '''
    
    Project developers: Hui Zhao, Sr. Juniper Engineer
                        Hui.Zhao@westcongroup.com
                        
                        Nikko Hayden, Integration Technican II
                        Nikko.Hayden@westcongroup.com
                        Injectnique@gmail.com
    
    project Github: https://Github.com/Injectnique/DCAP-Testing
                    ( Private Repo, invite only )
                    
    Project Documentation: .\DCAP-Testing\PhoneProvision\Common\Documentation\
    
    '''


#######################################################################
''' FTP SPECIFIC FUNCTIONS '''

def FTP_Login(FTP_Server='127.0.0.1', Username='anonymous', Password=''):
    ''' Log in to FTP server 
    
    + FTP authentication input
    - Username is string; anonymous is default;
    - Password is string; blank is default for anonymous login; '''
    global ftp
    ftp = FTP(FTP_Server)
    try:
        ftp.login(Username, Password)
        print '[+] Successfully logged in to', str(FTP_Server)+'!'
    except:
        if Username.lower() == 'anonymous':
            print '[!] Anonymous login is not enabled on this server'

def FTP_Upload(Path, File):
    global ftp
    try:
        ftp.storbinary('STOR '+File, open(File, 'rb'))
        print '[+] Successfully uploaded', str(File)+'!'
    except:
        print '[!] Got excepting while attempting to upload\n[!]', str(File), '\n'
                
def FTP_Download(File, Mode='binary'):
    ''' Mode is ascii or binary '''
    global ftp
    localfile = open(File, 'wb')
    if Mode.lower() == 'ascii':
        ftp.retrlines('RETR ' + File, localfile.write)
    elif Mode.lower() == 'binary':
        ftp.retrbinary('RETR ' + File, localfile.write, 1024)
    localfile.close()
    
def FTP_Directory_Listing(Debug=False):
    global ftp
    try:
        Directory_Listing = ftp.nlst()
    except ftplib.error_perm, resp:
        if str(resp) == '550 No files found':
            print 'No files in this directory'
        else:
            raise
    if Debug == True:
        print '\n[+] DIR'
        for file in Directory_Listing:
            print file
        print '\n'
    return Directory_Listing

def FTP_Quit():
    global FTP
    ftp.quit()

#######################################################################
''' DHCP SCOPE, LEASE, BOOT CODE SPECIFIC FUNCTIONS '''


def Set_DHCP_Scope_Option(Remote_Server='127.0.0.1', SSH_Port=9001,
                          Username='anonymous', Password='', 
                          ScopeID='192.168.10.0',  
                          Option_Code=None,
                          Option_Type='STRING',
                          Option_Value=None,
                          TK_Customer_Option=None,
                          Debug=False):
    ''' Set DHCP Server scope option
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + DHCP paramaters
    - ScopeID is string; Input IPv4 string of scope to export - eg 192.168.10.0;
      This must be a valid scope;
    - Option_Code is integer; Input boot option code; eg 66
    - Option_Value is 
    - Option_Type is string; See Option Type Notes for details; Most input will be STRING;
    
    + Option Type Notes
    - STRING; Input string; eg ftp://anonymous:@10.200.2.8/CLINK-FW-4.10/; Recommended input
    - IPADDRESS; Input IPv4; eg 192.168.5.20
    - DWORD; Input 32-bit integer; range: 0 through 4294967295 decimal; eg 3600
    
    + Notes
    - Set 1 scope option up at a time for a given subnet
    - netsh dhcp server scope 10.100.11.0 set optionvalue 160 STRING HelloWorld
    - TK_Customer_Option is input to be added if TK option menu for customer is set;
      We will override function with the required settings for the value

    return True on success; return False or None on exception '''
    
    
    if not TK_Customer_Option == None:
        # User selected pre-set TK option menu for customer
        pass #****************
    
    # Verify user input required parameters for scope
    if Option_Code or Option_Value == None:
        print '\n[!] You must specify Option_Code and Option_Value for function Set_DHCP_Scope_Option()'
        print '[+] Option_Code: ', Option_Code
        print '[+] Option_Value: ', Option_Value
        #return None
    
    # Sanitize Option_Code
    Option_Code = int(Option_Code)
    if len(str(Option_Code)) > 3:
        print '\n[!] Option_Code is too big of an integer; try 1 - 999'
        return False
    
    # Sanitize Option_Type
    Option_Type = Option_Type.upper()
    if not Option_Type == 'STRING' or not Option_Type == 'IPADDRESS' or not Option_Type == 'DWORD':
        print '\n[!] Invalid input for Option_Type - using STRING as type'
        Option_Type = 'STRING'
    
    # Sanitize ScopeID IPv4
    if not len(Remote_Server.split('.')) == 4:
        print '[!] Invalid ScopeID - Please input IPv4 subnet - eg 192.168.10.0'
    
    if Debug == True:
        print '[+] Connecting to %s using SSH on port %d' % (Remote_Server, int(SSH_Port))
    
    # Create SSH session
    ssh = paramiko.SSHClient()
    
    # Ignore fingerprint
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    # Start using SSH session (connect)
    ssh.connect(Remote_Server, username=Username, password=Password, port=int(SSH_Port))
    
    # NetSH command
    NetSH_DHCP_Export_Command = 'netsh dhcp server scope %s set optionvalue %d %s %s' % (ScopeID, Option_Code, Option_Type, Option_Value)

    # exec DHCP export command
    stdin_, stdout_, stderr_ = ssh.exec_command(NetSH_DHCP_Export_Command)
    stdin_.close()

    DHCP_Option_Value_Output = stdout_.read()

    if Debug == True:
        print '\n[+] NetSH DHCP Option Values Command:\n[+]', DHCP_Option_Value_Output
    
    if 'Command completed'.upper() in DHCP_Option_Value_Output.upper() or 'successfully'.upper() in DHCP_Option_Value_Output.upper():
        if Debug == True:
            print '[+] Successfully set option %s to value %s on subnet %s' % (Option_Code, Option_Value, ScopeID)
        return True
    
    elif 'does not exist'.upper() in DHCP_Option_Value_Output.upper():
        if Debug == True:
            print '\n[!] Error setting option %s to value %s on subnet %s' % (Option_Code, Option_Value, ScopeID)
            print '[!] Error appears to be option code input'
        return False
    
    elif 'either incomplete or invalid'.upper() in DHCP_Option_Value_Output.upper():
        if Debug == True:
            print '\n[!] Error setting option %s to value %s on subnet %s' % (Option_Code, Option_Value, ScopeID)
            print '[!] Error appears to be incomplete or invalid command'
        return False

    else:
        if Debug == True:
            print '\n[!] Unidentified error while setting option %s to value %s on subnet %s' % (Option_Code, Option_Value, ScopeID)
        return False


def Get_DHCP_Scope_Options(Remote_Server='127.0.0.1', SSH_Port=9001,
                           Username='anonymous', Password='',
                           ScopeID='192.168.10.0',  Debug=False):
    ''' Get DHCP Server scope options
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + DHCP paramaters
    - ScopeID is string; Input IPv4 string of scope to export - eg 192.168.10.0;
      This must be a valid scope. If left blank file exports the Scopes, but no leases.
    - Return_Specific_Option_Code is boolean; True to look for specified Option Code
    - Specific_Option_Code is integer; Input specific option code to return
      
    + Notes
    - Scope_Options is list; List contains subnet scope option values; 
      return: [ (Option Code, Option Value), .. ]
    - Remote_Server should have DHCP and SSH running
    - If option code is set but value is blank then we will ignore option all together
    
    return Scope_Options '''
    
    # Sanitize Remote_Server IPv4
    if not len(Remote_Server.split('.')) == 4:
        print '[!] Invalid Remote_Server - Please input IPv4 of DHCP server - eg 192.168.5.10'
        return None
    
    # Sanitize ScopeID IPv4
    if not len(ScopeID.split('.')) == 4:
        print '[!] Invalid ScopeID - Please input IPv4 subnet - eg 192.168.10.0'
        return None
    
    if Debug == True:
        print '[+] Connecting to %s using SSH on port %d' % (Remote_Server, int(SSH_Port))
    
    # Create SSH session
    ssh = paramiko.SSHClient()
    
    # Ignore fingerprint
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    # Start using SSH session (connect)
    try:
        ssh.connect(Remote_Server, username=Username, password=Password, port=int(SSH_Port))
    except Exception, e:
        if Debug == True:
            print '\n[!] Got exception while connecting to DHCP server with SSH'
            print '[!] Exception message:\n', str(e), '\n'
        return None
    
    
    # NetSH command
    NetSH_DHCP_Export_Command = 'netsh dhcp server scope %s show optionvalue' % ScopeID

    # exec DHCP export command
    stdin_, stdout_, stderr_ = ssh.exec_command(NetSH_DHCP_Export_Command)
    stdin_.close()

    DHCP_Option_Value_Output = stdout_.read()#.splitlines()

    if Debug == True:
        print '\n[+] NetSH DHCP Option Values Command:\n[+]', DHCP_Option_Value_Output
    
    # return: [ (Option Code, Option Value), .. ]
    Regex_Option_Value = 'OptionId : (\d{1,3})[. \r\n\tA-Za-z0-9:@/=-_+]+?Option Element Value = ([^\n]+)'
    #'OptionId : (\d{1,3})[ \r\nA-Za-z0-9:=@/]+?Option Element Value = ([^\n]+)'
    
    Scope_Options = re.findall(Regex_Option_Value, DHCP_Option_Value_Output)
    
    # Sanitize newline characters
    Sanitized_Scope_Options = []
    for option in Scope_Options:
        Sanitized_Scope_Options.append([option[0], option[1].replace('\r', '').replace('\n', '').replace('\t', '')])
    
    if Debug == True:
        print '[+] We found %d Scope Options on Subnet: %s' % (len(Sanitized_Scope_Options), ScopeID)
        print '\n', Sanitized_Scope_Options, '\n'

    #import pdb;pdb.set_trace()
    
    #return Scope_Options
    return Sanitized_Scope_Options



def Get_Unreachable_Hosts(IPs=[], Remote_Server='127.0.0.1', SSH_Port=9001,
                          Username='anonymous', Password='', 
                          ScopeID='192.168.10.0', Debug=False):
    ''' Input list of IPs you are able to reach on a subnet,
    We will return a list of the hosts you could not reach that have an active lease
    
    + Function parameters
    - IPs is list; Input list of IPs that you were able to PING
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + DHCP export parameters
    - ScopeID is string; Input subnet scope ID you wish to compare IPs to
    
    + Notes
    - This function can be used to identify phones that lost network connectivity
      but did provision, so they would have an active DHCP lease still
    - Unreachable_IPs is list; List to be filled with unreachable phones;
      Data structure: [  [IP, MAC], [IP, MAC], ...  ]
    
    return Unreachable_IPs '''
    Unreachable_IPs = [] # Placeholder variable
    
    if Debug == True:
        print '[+] We have %s IPs we could reach, comparing against active leases for subnet' % len(IPs)
        print '[+] DHCP Server:', Remote_Server
    
    # Get active leases from subnet as list: [ [IP, Mask, MAC, Lease Expire Time],.. ]
    Active_Leases = Get_Active_DHCP_Lease_Information(Remote_Server=Remote_Server,
                                                      SSH_Port=SSH_Port,
                                                      Username=Username,
                                                      Password=Password,
                                                      ScopeID=ScopeID,
                                                      Debug=Debug)
    
    if Debug == True:
        print '[+] We have %s active leases for subnet %s' % (len(Active_Leases), ScopeID)
    
    for lease in Active_Leases:
        if not lease[0] in IPs:
            Unreachable_IPs.append([lease[0], lease[2]]) # [IP, MAC]
    
    if Debug == True:
        print '[+] %d Unreachable IPs:\n' % len(Unreachable_IPs)
        print Unreachable_IPs, '\n'
    
    return Unreachable_IPs


def Get_Unreachable_MAC(IPs=[], Remote_Server='127.0.0.1', SSH_Port=9001,
                          Username='anonymous', Password='',
                          ScopeID='192.168.10.0', Debug=False):
    ''' Input list of IPs you are able to reach on a subnet,
    We will return a list of the hosts you could not reach that have an active lease

    + Function parameters
    - IPs is list; Input list of IPs that you were not able to PING

    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service

    + DHCP export parameters
    - ScopeID is string; Input subnet scope ID you wish to compare IPs to

    + Notes
    - This function can be used to identify phones that lost network connectivity
      but did provision, so they would have an active DHCP lease still
    - Unreachable_IPs is list; List to be filled with unreachable phones;
      Data structure: [  [IP, MAC], [IP, MAC], ...  ]

    return Unreachable_IPs '''
    Unreachable_MACs = [] # Placeholder variable

    if Debug == True:
        print '[+] We have %s IPs we could reach, comparing against active leases for subnet' % len(IPs)
        print '[+] DHCP Server:', Remote_Server

    # Get active leases from subnet as list: [ [IP, Mask, MAC, Lease Expire Time],.. ]
    Active_Leases = Get_Active_DHCP_Lease_Information(Remote_Server=Remote_Server,
                                                      SSH_Port=SSH_Port,
                                                      Username=Username,
                                                      Password=Password,
                                                      ScopeID=ScopeID,
                                                      Debug=Debug)

    if Debug == True:
        print '[+] We have %s active leases for subnet %s' % (len(Active_Leases), ScopeID)

    for lease in Active_Leases:
        if lease[0] in IPs:
            Unreachable_MACs.append([lease[0], lease[2]]) # [IP, MAC]

    if Debug == True:
        print '[+] %d Unreachable IPs:\n' % len(Unreachable_MACs)
        print Unreachable_MACs, '\n'

    return Unreachable_MACs

def Get_Active_DHCP_Lease_Information(Remote_Server='127.0.0.1', SSH_Port=9001,
                                      Username='anonymous', Password='', 
                                      ScopeID='192.168.10.0',  Debug=False):
    ''' Get active leases on a subnet from a specified DHCP server using NetSH & SSH
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + DHCP export paramaters
    - ScopeID is string; Input IPv4 string of scope to export - eg 192.168.10.0;
      This must be a valid scope. If left blank file exports the Scopes, but no leases.
    - Active_Leases is list; List contains device lease information; eg [IP, Mask, MAC, Lease Expire Time]
    
    + Notes
    - Remote_Server should have DHCP and SSH running
    - Active_Leases: [ [Device_IP, Device_Subnet_Mask, Device_MAC, Device_Lease_Expire_Time],.. ]
    
    return Active_Leases on success;
    return False or None on excpetion/fail; '''
    
    Active_Leases = [] # [ [IP, Mask, MAC, Lease Expire Time],.. ]
    
    # Sanitize Remote_Server IPv4
    if not len(Remote_Server.split('.')) == 4:
        print '[!] Invalid Remote_Server - Please input IPv4 of DHCP server - eg 192.168.5.10'
        return None
    
    # Sanitize ScopeID IPv4
    if not len(ScopeID.split('.')) == 4:
        print '[!] Invalid ScopeID - Please input IPv4 subnet - eg 192.168.10.0'
        return None
    
    if Debug == True:
        print '[+] Connecting to %s using SSH on port %d' % (Remote_Server, int(SSH_Port))
    
    # Create SSH session
    ssh = paramiko.SSHClient()
    
    # Ignore fingerprint
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    # Start using SSH session (connect)
    try:
        ssh.connect(Remote_Server, username=Username, password=Password, port=int(SSH_Port))
    except Exception, e:
        if Debug == True:
            print '\n[!] Got exception while connecting to DHCP server with SSH'
            print '[!] Exception message:\n', str(e), '\n'
        return None
    
    # Create SFTP session
    #sftp = ssh.open_sftp()
    
    # Powershell attempt (not-working)
    #PS_Command = 'powershell /c "Get-DHCPServerv4Lease -ScopeID %s | Format-Table -Property IPAddress,ClientId | Out-File C:\Windows\Temp\PS_Out_%s.txt -Encoding ascii"' % (ScopeID, ScopeID)
    
    # NetSH attempt (working)
    # netsh dhcp server scope 10.100.11.0 show clients
    # netsh dhcp server \\ops-na-ddi-02 scope 10.100.11.0 show clients
    # netsh dhcp server \\ops-na-ddi-02 show scope 0 > C:\\Windows\\Temp\\PS_Out.txt
    NetSH_DHCP_Export_Command = 'netsh dhcp server scope %s show clients' % ScopeID #> C:\\Windows\\Temp\\PS_Out.txt' % ScopeID

    # exec DHCP export command
    # This will export DHCP information to PS_Out.txt
    stdin_, stdout_, stderr_ = ssh.exec_command(NetSH_DHCP_Export_Command)
    stdin_.close()

    DHCP_Export_Output = stdout_.read().splitlines()

    if Debug == True:
        print '\n[+] NetSH DHCP Export Command:\n[+]', NetSH_DHCP_Export_Command#, '\n', '-'*10
        #for line in DHCP_Export_Output:
        #    print line
        #print '\n', '-'*10, '\n'
    
    # Get directory listing output from Remote_Server
    # We are looking for PS_Out.txt (DHCP Export file)
    #stdin, stdout, stderr = ssh.exec_command('cmd.exe /c "cd C:\\Windows\\Temp && dir"')
    #stdin.close()
 
    #if Debug == True:
    #    print '[+] %d files in temp directory!' % len(stdout.read().splitlines())
    # ***** Cant do .read() x2
 
##    # Iterate for old export files
##    for file in stdout.read().splitlines():
##         
##        # Parse filename from line
##        Filename = file.split(' ')[::-1][0]
##         
##        # Delete old export files
##        if 'PS_Out'.upper() in str(Filename).upper():
##            if Debug == True:
##                print '[+] Found DHCP export file in temp directory: %s' % Filename
##            
##            # Use SFTP copy this file to local system
##            
##            #sftp.get(r'C:\Windows\Temp\PS_Out.txt', r'C:\Windows\Temp\PS_Out.txt')
##            sftp.get('PS_Out.txt', r'C:\Windows\Temp\PS_Out.txt')
##            if Debug == True:
##                print '[+] Successfully copied DHCP Export file via SFTP to local system'
##            #sftp.get('putty.exe', r'C:\\Python27\\putty.exe')
##    
##    # Read DHCP export file from local system
##    if Debug == True:
##        print '[+] Reading DHCP Export file from local system'
##    DHCP_Export = open(r'C:\Windows\Temp\PS_Out.txt', 'r')
##    DHCP_Export_Data = DHCP_Export.readlines()
##    DHCP_Export.close()
##    
##    # Delete DHCP export file from local system
##    os.remove(r'C:\Windows\Temp\PS_Out.txt')
    
    # Iterate DHCP export data
    #for line in DHCP_Export_Data:
    for line in DHCP_Export_Output:
        
        # Placeholder variables to be parsed
        Device_IP = None
        Device_Subnet_Mask = '0.0.0.0'
        Device_MAC = '000000000000'
        Device_Lease_Expire_Time = None
        
        try: # return: (IPv4, Mask, MAC, Lease Expire Time)
            Device_Lease_Info = re.findall(Regex_NetSH_DHCP_Export, line)[0]
            Device_IP = Device_Lease_Info[0].replace(' ', '')
            Device_Subnet_Mask = Device_Lease_Info[1].replace(' ', '')
            Device_MAC = Device_Lease_Info[2].replace(' ', '').replace('-', '').upper()
            Device_Lease_Expire_Time = Device_Lease_Info[3]
        except:
            # Try Alt regexp
            # Alt regexp has no lease time value
            # return: (IPv4, Mask, MAC)
            try:
                Device_Lease_Info = re.findall(Alt_Regex_NetSH_DHCP_Export, line)[0]
                Device_IP = Device_Lease_Info[0].replace(' ', '')
                Device_Subnet_Mask = Device_Lease_Info[1].replace(' ', '')
                Device_MAC = Device_Lease_Info[2].replace(' ', '').replace('-', '').upper()
                Device_Lease_Expire_Time = '0/0/0 00:00:00 ??'
            except:
                try: # Just get the IP address then
                    Device_Lease_Info = re.findall(Regex_IP, line)[0]
                    Device_IP = Device_Lease_Info
                    Device_Subnet_Mask = '0.0.0.0'
                    Device_MAC = '000000000000'
                    Device_Lease_Expire_Time = '0/0/0 00:00:00 ??'
                except:
                    if line[0:10] == ScopeID[0:10]:
                        print '\n[!] Error parsing a line that looks like it should have been parsed!'
                        print '[!] Line:', str(line), '\n'
        
        # At this point we should have IP, Mask, and MAC at MINIMUM
        
        if not Device_IP == None:
            # if we have IP

            # Sanitize IP from lease
            if not Device_IP in GlobalVariables.Banned_IPs:
                if not Device_IP.split('.')[3] == '0':
                    if Debug == True:
                        print '[+] Parsed %s:%s from DHCP Export' % (Device_IP, Device_MAC)
                    # Append to Active_Leases list
                    Active_Leases.append([Device_IP, Device_Subnet_Mask, Device_MAC, Device_Lease_Expire_Time])
    
    if Debug == True:
        print '\n[+] Got %d active leases from DHCP Server!' % len(Active_Leases)
    
    # [ [Device_IP, Device_Subnet_Mask, Device_MAC, Device_Lease_Expire_Time],.. ]
    return Active_Leases



def Get_All_Active_DHCP_Lease_Information(Remote_Server='127.0.0.1', SSH_Port=9001,
                                          Username='anonymous', Password='', 
                                          Scopes=['192.168.10.0', '192.168.20.0'],
                                          Debug=False):
    ''' Get all active leases on a specified DHCP server using NetSH & SSH
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + DHCP export paramaters
    - Scopes is list; Input list containing IPv4 string of scopes to export;
      eg: Scopes=['192.168.10.0', '192.168.20.0'];
    - Active_Leases is list; List contains device lease information; eg [IP, Mask, MAC, Lease Expire Time]
    
    + Notes
    - Remote_Server should have DHCP and SSH running
    - All_Active_Leases; [ [Device_IP, Device_Subnet_Mask, Device_MAC, Device_Lease_Expire_Time],.. ]
    
    return Active_Lease_IPs on success;
    return False or None on excpetion/fail; '''
    
    All_Active_Leases = [] # Placeholder variable to be filled with IPv4
    
    if Debug == True:
        print '[+] About to iterate %d scopes' % len(Scopes)
        for i in Scopes:
            print '[+] %s' % i
        print '\n'
    
    # Iterate subnet IPv4 elements
    for scope in Scopes:
        # eg 192.16.10.0
        
        # Sanitize IPv4 for scope
        if not len(scope.split('.')) == 4:
            print '\n[!] Invalid Ipv4 for scope: %s' % scope
            print '''[!] Please input IPv4s as list - eg ['192.168.10.0', '192.168.20.0']'''
            break
        
        if Debug == True:
            print '[+] Attempting to get IP addresses from scope: %s' % scope
        
        # Get leases from scope
        Active_Leases = Get_Active_DHCP_Lease_Information(Remote_Server=Remote_Server, 
                                                          SSH_Port=SSH_Port,
                                                          Username=Username, 
                                                          Password=Password, 
                                                          ScopeID=scope, 
                                                          Debug=Debug)

        if not Active_Leases == None:
            for lease in Active_Leases:
                All_Active_Leases.append(lease)#[0])
            if Debug == True:
                print '[+] Got %d IP addresses from scope %s' % (len(All_Active_Leases), scope)
        
    if Debug == True:
        print '[+] Got %d IP addresses from %d scopes!' % (len(All_Active_Leases), len(Scopes))
    
    # Active_Leases: [ [Device_IP, Device_Subnet_Mask, Device_MAC, Device_Lease_Expire_Time],.. ]
    return All_Active_Leases




def Delete_Active_Lease(Remote_Server='127.0.0.1', SSH_Port=9001,
                        Username='anonymous', Password='', 
                        ScopeID='192.168.10.0', Lease_to_Delete='192.168.10.10',
                        Debug=False):
    ''' Delete a specified lease from a specified scope on the DHCP server
    
    + Remote server authentication parameters
    - Remote_Server is string; Input IPv4 of DHCP/SSH server
    - Username is string; Input username for SSH authentication
    - Password is string; Input password for SSH authentication
    - SSH_Port is integer; Input port number for SSH service
    
    + Lease_to_Delete is string or list; Input desired IPv4 lease to delete;
      If input is list we will iterate list and delete all values
    
    + Notes
    - Remote_Server should have DHCP and SSH running
    
    return True on success
    return False or None on exception/error  '''
    
    # Sanitize Remote_Server IPv4
    if not len(Remote_Server.split('.')) == 4:
        print '[!] Invalid Remote_Server - Please input IPv4 of DHCP server - eg 192.168.5.10'
        return None
    
    # Sanitize ScopeID IPv4
    if not len(ScopeID.split('.')) == 4:
        print '[!] Invalid ScopeID - Please input IPv4 subnet - eg 192.168.10.0'
        return None
    
    ## Sanitize Lease_to_Delete IPv4
    #if not len(Lease_to_Delete.split('.')) == 4:
    #    print '[!] Invalid Lease_to_Delete - Please input IPv4 - eg 192.168.10.10'
    #    return None
    
    if Debug == True:
        print '[+] Connecting to %s using SSH on port %d' % (Remote_Server, int(SSH_Port))
    
    # Create SSH session
    ssh = paramiko.SSHClient()
    
    # Ignore fingerprint
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    # Start using SSH session (connect)
    try:
        ssh.connect(Remote_Server, username=Username, password=Password, port=int(SSH_Port))
    except Exception, e:
        if Debug == True:
            print '\n[!] Got exception while connecting to DHCP server with SSH'
            print '[!] Exception message:\n', str(e), '\n'
        return None
    
    # Check if Lease_to_Delete is string or list
    if type(Lease_to_Delete) == list:
        # User supplied list of IP addresses
        for IP_Address in Lease_to_Delete:
            # NetSH command
            NetSH_DHCP_Lease_Delete_Command = 'netsh dhcp server scope %s delete lease %s' % (ScopeID, IP_Address)
            
            # exec DHCP export command
            stdin_, stdout_, stderr_ = ssh.exec_command(NetSH_DHCP_Lease_Delete_Command)
        stdin_.close()
            
    elif type(Lease_to_Delete) == str:
        # NetSH command
        NetSH_DHCP_Lease_Delete_Command = 'netsh dhcp server scope %s delete lease %s' % (ScopeID, Lease_to_Delete)
    
        # exec DHCP export command
        stdin_, stdout_, stderr_ = ssh.exec_command(NetSH_DHCP_Lease_Delete_Command)
        stdin_.close()

    NetSH_DHCP_Lease_Delete_Command_Output = stdout_.read()#.splitlines()

    if Debug == True:
        print '\n[+] NetSH DHCP Lease Delete Command:\n[+]', NetSH_DHCP_Lease_Delete_Command_Output
    
    Lease_Delete_Success = None
    # **** Verify success std output & not error message here
    #Regex_Option_Value = ''
    #Scope_Options = re.findall(Regex_Option_Value, NetSH_DHCP_Lease_Delete_Command_Output)
    
    if Lease_Delete_Success == True:
        if Debug == True:
            print '[+] Successfully deleted lease %s' % Lease_to_Delete
        return True
    
    else:
        if Debug == True:
            print '\n[!] Got exception while deleting lease %s' % Lease_to_Delete
        return False
    
def get_availableip(railnumber, phonesum_onthisrail):

    # Do IP Ping on this Rail from 192.168.x.11~192.168.x.maxphoneno_onthisrail
    # Input 1st parameter as the rail number, and the 2nd parameter as how many phones tested on this rail this time
    # Return list of available ip address list

        # define and construct the ip range according to DHCP ip pool setting
        iprange_list = []
        DHCPPoolRange = GlobalVariables.DHCPPoolEnd - GlobalVariables.DHCPPoolStart
        for j in range(GlobalVariables.DHCPPoolStart,GlobalVariables.DHCPPoolEnd+1,1):
            iprange_list.append("192.168."+str(railnumber*10)+"."+str(j))

        #Try 2 times
        for loop in range(2):

            available_list = []
            print ("\nThis is the " + str(loop+1) + "times check ip address connectivity")
            for j in range(DHCPPoolRange+1):
                response = os.system("ping -n 1 -l 1 -w 10 " + iprange_list[j])
                if response == 0:
                    available_list.append(iprange_list[j])

                if len(available_list) == phonesum_onthisrail:
                    print ("IP scanning ok, continue")
                    break
            if len(available_list) == phonesum_onthisrail:
                break
        print ("Totally there are "+str(len(available_list))+" phones detected. "+str(phonesum_onthisrail-len(available_list))+" missing")
        # print ("The available ip addresses are:",available_list)
        return available_list

def detect_status_ping(old_list):
# Do IP Ping on list element, If one element ping is OK, remove it from the list.
# Return True if all elements ping are OK
# If some phones are NOK, Return a list of IP addresses of the elements whose ping OK

    new_list = old_list
    for loop in range(5):
        print "Start the %d check!" % loop
        count = 0
        list_ok = []
        for i in range(len(old_list)):
            response = os.system("ping -n 1 -l 1 -w 10 " + old_list[i])
            if response == 0:
                list_ok.append(old_list[i])
                count = count +1

        if count == len(new_list):
            print ("IP scanning ok, continue")
            break
        else:
            time.sleep(60)

    if count == len(new_list):
        return True
    else:
        for j in range(len(list_ok)):
            old_list.remove(list_ok[j])

        return list_ok
        # return new_list

#######################################################################








