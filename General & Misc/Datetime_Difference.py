#!/usr/bin/python
# Filename: Datetime_Difference.py
# -*- coding: utf-8 -*-

import datetime

Time_A = datetime.datetime.now()
Time_B = datetime.datetime.now()
Time_Difference = Time_B - Time_A

'''
datetime.timedelta(0, 8, 562000)
>>> divmod(c.days * 86400 + c.seconds, 60)
(0, 8)      # 0 minutes, 8 seconds
'''
