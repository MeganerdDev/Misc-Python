#!/usr/bin/python
# Filename: HeliumSample.py
# -*- coding: utf-8 -*-

'''
Sample of Helium library usage

Note: Helium requires licensing
'''

''' Imports '''
import os
import platform
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from helium import api

# Optional imports
#from selenium.webdriver.common.by import By
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.support.ui import Select
#from selenium.common.exceptions import NoSuchElementException
#from selenium.common.exceptions import NoAlertPresentException
#import unittest


''' Settings '''
# Current path (where file launched)
Current_Path = os.path.dirname(os.path.abspath(__file__))

# Host OS
OS = platform.system()

# PhantomJS binary path
PhantomJS_Path = Current_Path+'/PhantomJS/phantomjs'
#PhantomJS_Path = Current_Path+r'/phantomjs/phantomjs'

Protocol = 'HTTPS'
Example_URL = Protocol+'://reddit.com/r/The_Donald'

''' Functions '''
def PhantomJS_Open_URL(URL=Example_URL, Debug=False):
    ''' Open URL in browser using PhantomJS
    Simulate clicking in a button in the browser '''
    Driver = webdriver.PhantomJS(executable_path=PhantomJS_Path)
    Driver.set_window_size(1920, 1080)
    Driver.get(Example_URL)
    
    # Set driver to Helium
    api.set_driver(Driver)
    
    # Find submit link button
    Helium_Submit_btn = api.find_all(api.Button('Submit'))
    Helium_Submit_btn_ = sorted(Helium_Submit_btn, key=lambda btn: btn.y)[0]

    # Click submit link button
    api.click(Helium_Submit_btn_.top_left)
    
    return
    
                        

if __name__ == '__main__':
    PhantomJS_Open_URL()        

